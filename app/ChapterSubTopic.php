<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChapterSubTopic extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'topic_id'
    ];

    public function topics()
    {
        return $this->belongsTo("\App\ChapterTopic");
    }
}
