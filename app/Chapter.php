<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'subject_id'
    ];

    public function subjects()
    {
        return $this->belongsTo("\App\Subject", "subject_id");
    }

    public function chapterTopics()
    {
        return $this->hasMany("\App\ChapterTopic")->with('chapterSubTopics');
    }

    public function questions()
    {
        return $this->hasMany("\App\QuestionBank", 'chapter_id');
    }
}
