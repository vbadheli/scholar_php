<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionOption extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question_id','option'
    ];

    public function questions()
    {
        return $this->belongsTo("\App\QuestionBank", "question_id");
    }
}
