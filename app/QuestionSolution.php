<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionSolution extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question_id','description'
    ];

    public function questions()
    {
        return $this->belongsTo("\App\QuestionBank", "question_id");
    }
}
