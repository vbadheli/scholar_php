<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChapterTopic extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'chapter_id'
    ];

    public function chapters()
    {
        return $this->belongsTo("\App\Chapter");
    }

    public function chapterSubTopics()
    {
        return $this->hasMany("\App\ChapterSubTopic",'topic_id');
    }

    public function questions()
    {
        return $this->hasMany("\App\QuestionBank",'chapter_topic_id');
    }

    public function questionCount()
    {
        return $this->hasMany("\App\QuestionBank",'chapter_topic_id');
    }
}
