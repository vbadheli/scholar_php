<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Subject;
use App\Chapter;
use App\ChapterTopic;
use App\ChapterSubTopic;
use App\RandomSelectionSetting;
use App\QuestionBank;
use App\TestQuestion;
use Illuminate\Support\Facades\DB;
use App\Exports\AppExp;
use Maatwebsite\Excel\Facades\Excel;

class AppController extends Controller
{
    /**
     * Retrieve the user for the given ID.
     *
     * @param  int  $id
     * @return Response
     */
    public function showAllData(Request $req, $type)
    {
        //if($req->has('order_col'))
            //dd($req->all());
        if($type == "subject")
        {
            if($req->page > 0)
                $data = Subject::paginate(5);
            else
                $data = Subject::get();
        }
        else if($type == "chapter")
        {
            if($req->page > 0)
                if($req->has('order_col') && $req->has('order_by'))
                    $data = Chapter::with("subjects", 'chapterTopics')->orderBy($req->order_col, $req->order_by)->paginate(5);
                else
                    $data = Chapter::with("subjects", 'chapterTopics')->paginate(5);
            else
                if($req->has('order_col') && $req->has('order_by'))
                    $data = Chapter::with("subjects", 'chapterTopics')->orderBy($req->order_col, $req->order_by)->get();
                else
                    $data = Chapter::with("subjects", 'chapterTopics')->get();
        }
        else if($type == "topic")
        {
            if($req->page > 0)
                $data = ChapterTopic::with("chapters")->paginate(5);
            else
                $data = ChapterTopic::with("chapters")->get();
        }
        else if($type == "subtopic")
        {
            if($req->page > 0)
                $data = ChapterSubTopic::with("topics")->paginate(5);
            else
                $data = ChapterSubTopic::with("topics")->get();
        }
        else if($type == "question")
        {
            if($req->page > 0)
            {
                if($req->has('order_col') && $req->has('order_by'))
                    $data = \App\QuestionBank::with("subject","chapter")->orderBy($req->order_col, $req->order_by)->paginate(5);
                else
                    $data = \App\QuestionBank::with("subject","chapter")->paginate(5);
            }
            else
                if($req->has('order_col') && $req->has('order_by'))
                    $data = \App\QuestionBank::with("subject","chapter")->orderBy($req->order_col, $req->order_by)->get();
                else
                    $data = \App\QuestionBank::with("subject","chapter")->get();
        }
        else 
        {
            $data=[];
        }
        $content=['data'=>$data];
        $status = 200;
        $value="application/json";
        return (new Response($content, $status))->header('Content-Type', $value);
    }

    public function addData(Request $request)
    {
        if($request->type == "subject")
        {
            $subj = new Subject($request->all());
            // dd($subj->save());
            if($subj->save())
            {

                $content=['message'=>"subject added", 'data'=>$subj];
                $status = 200;
                $value="application/json";
                return (new Response($content, $status))->header('Content-Type', $value);
            }
            else{
                
                $content=['message'=>"unable to add"];
                $status = 400;
                $value="application/json";
                return (new Response($content, $status))->header('Content-Type', $value);
            }
        }
        else if($request->type == "chapter")
        {
            $subj = new Chapter($request->all());
            if($subj->save())
            {

                $content=['message'=>"chapter added", 'data'=>$subj];
                $status = 200;
                $value="application/json";
                return (new Response($content, $status))->header('Content-Type', $value);
            }
            else{
                
                $content=['message'=>"unable to add"];
                $status = 400;
                $value="application/json";
                return (new Response($content, $status))->header('Content-Type', $value);
            }
        }
        else if($request->type == "topic")
        {
            $subj = new ChapterTopic($request->all());
            if($subj->save())
            {

                $content=['message'=>"topic added", 'data'=>$subj];
                $status = 200;
                $value="application/json";
                return (new Response($content, $status))->header('Content-Type', $value);
            }
            else{
                
                $content=['message'=>"unable to add"];
                $status = 400;
                $value="application/json";
                return (new Response($content, $status))->header('Content-Type', $value);
            }
        }
        else if($request->type == "subtopic")
        {
            $subj = new ChapterSubTopic($request->all());
            if($subj->save())
            {

                $content=['message'=>"sub topic added", 'data'=>$subj];
                $status = 200;
                $value="application/json";
                return (new Response($content, $status))->header('Content-Type', $value);
            }
            else{
                
                $content=['message'=>"unable to add"];
                $status = 400;
                $value="application/json";
                return (new Response($content, $status))->header('Content-Type', $value);
            }
        }
        else if($request->type == "question")
        {
            $subj = new \App\QuestionBank($request->all());
            if($subj->save())
            {
                $option_a = new \App\QuestionOption(['question_id'=> $subj->id, 'option'=> $request->option_a]);
                $option_a->save();
                $option_b = new \App\QuestionOption(['question_id'=> $subj->id, 'option'=> $request->option_b]);
                $option_b->save();
                $option_c = new \App\QuestionOption(['question_id'=> $subj->id, 'option'=> $request->option_c]);
                $option_c->save();
                $option_d = new \App\QuestionOption(['question_id'=> $subj->id, 'option'=> $request->option_d]);
                $option_d->save();
                
                if($request->answer == 1)
                    $subj->answer = $option_a->id;
                else if($request->answer == 2)
                    $subj->answer = $option_b->id;
                else if($request->answer == 3)
                    $subj->answer = $option_c->id;
                else if($request->answer == 4)
                    $subj->answer = $option_d->id;
                    
                $subj->update();

                $solution = \App\QuestionSolution::insert(['question_id'=> $subj->id, 'description' => $request->solution]);

                $content=['message'=>"sub topic added", 'data'=>$subj];
                $status = 200;
                $value="application/json";
                return (new Response($content, $status))->header('Content-Type', $value);
            }
            else{
                
                $content=['message'=>"unable to add"];
                $status = 400;
                $value="application/json";
                return (new Response($content, $status))->header('Content-Type', $value);
            }
        }

        else
        {
                
            $content=['message'=>"invalid request"];
            $status = 400;
            $value="application/json";
            return (new Response($content, $status))->header('Content-Type', $value);
        }
    }

    public function updateData(Request $request)
    {
        if($request->type == "subject")
        {
            $subj = Subject::find($request->id);
            // dd($subj->update($request->all()));
            // $subj->title 
            if($subj->update($request->all()))
            {

                $content=['message'=>"updated", 'data'=>$subj];
                $status = 200;
                $value="application/json";
                return (new Response($content, $status))->header('Content-Type', $value);
            }
            else{
                
                $content=['message'=>"unable to update"];
                $status = 400;
                $value="application/json";
                return (new Response($content, $status))->header('Content-Type', $value);
            }
        }
        else if($request->type == "chapter")
        {
            $subj = Chapter::find($request->id);
            if($subj->update($request->all()))
            {

                $content=['message'=>"updated", 'data'=>$subj];
                $status = 200;
                $value="application/json";
                return (new Response($content, $status))->header('Content-Type', $value);
            }
            else{
                
                $content=['message'=>"unable to update"];
                $status = 400;
                $value="application/json";
                return (new Response($content, $status))->header('Content-Type', $value);
            }
        }
        else if($request->type == "topic")
        {
            $subj = ChapterTopic::find($request->id);
            if($subj->update($request->all()))
            {

                $content=['message'=>"updated", 'data'=>$subj];
                $status = 200;
                $value="application/json";
                return (new Response($content, $status))->header('Content-Type', $value);
            }
            else{
                
                $content=['message'=>"unable to update"];
                $status = 400;
                $value="application/json";
                return (new Response($content, $status))->header('Content-Type', $value);
            }
        }
        else if($request->type == "subtopic")
        {
            $subj = ChapterSubTopic::find($request->id);
            if($subj->update($request->all()))
            {

                $content=['message'=>"updated", 'data'=>$subj];
                $status = 200;
                $value="application/json";
                return (new Response($content, $status))->header('Content-Type', $value);
            }
            else{
                
                $content=['message'=>"unable to update"];
                $status = 400;
                $value="application/json";
                return (new Response($content, $status))->header('Content-Type', $value);
            }
        }

        else
        {
                
            $content=['message'=>"invalid request"];
            $status = 400;
            $value="application/json";
            return (new Response($content, $status))->header('Content-Type', $value);
        }
    }

    public function showData($type, $id)
    {
        if($type == "subject")
        {
            $data = Subject::find($id);
        }
        else if($type == "chapter")
        {
            $data = Chapter::find($id);
        }
        else if($type == "topic")
        {
            $data = ChapterTopic::find($id);
        }
        else if($type == "subtopic")
        {
            $data = ChapterSubTopic::find($id);
        }
        $content=['data'=>$data];
        $status = 200;
        $value="application/json";
        return (new Response($content, $status))->header('Content-Type', $value);
    }

    public function deleteData(Request $request)
    {
        if($request->type == "subject")
        {
            $data = Subject::find($request->id);
        }
        else if($request->type == "chapter")
        {
            $data = Chapter::find($request->id);
        }
        else if($request->type == "topic")
        {
            $data = ChapterTopic::find($request->id);
        }
        else if($request->type == "subtopic")
        {
            $data = ChapterSubTopic::find($request->id);
        }
        if($data !=null && $data->delete())
        {
            $content=['data'=>"deleted"];
            $status = 200;
        }
        else{
            $content=['data'=>"unable to delete"];
            $status = 400;
        }
        $value="application/json";
        return (new Response($content, $status))->header('Content-Type', $value);
    }

    public function deleteAllData()
    {
        if($request->type == "subject")
        {
            $data = Subject::find($request->id);
        }
        else if($request->type == "chapter")
        {
            $data = Chapter::find($request->id);
        }
        else if($request->type == "topic")
        {
            $data = ChapterTopic::find($request->id);
        }
        else if($request->type == "subtopic")
        {
            $data = ChapterSubTopic::find($request->id);
        }
        if($data !=null && $data->delete())
        {
            $content=['data'=>"deleted"];
            $status = 200;
        }
        else{
            $content=['data'=>"unable to delete"];
            $status = 400;
        }
        $value="application/json";
        return (new Response($content, $status))->header('Content-Type', $value);
    }

    public function getSubjectChapters($subject_id)
    {
        $data = Chapter::where('subject_id', $subject_id)->get();
        $content=['data'=>$data];
        $status = 200;
        $value="application/json";
        return (new Response($content, $status))->header('Content-Type', $value);
    }

    public function getChapterTopics($chapter_id)
    {
        $data = ChapterTopic::where('chapter_id', $chapter_id)->get();
        $content=['data'=>$data];
        $status = 200;
        $value="application/json";
        return (new Response($content, $status))->header('Content-Type', $value);
    }

    public function getTopicSubTopics($topic_id)
    {
        $data = ChapterSubTopic::where('topic_id', $topic_id)->get();
        $content=['data'=>$data];
        $status = 200;
        $value="application/json";
        return (new Response($content, $status))->header('Content-Type', $value);
    }

    public function generateTp(Request $request)
    {

        // no.of question from each difficulty level to select randomly based on percent in settings table
        $settings = RandomSelectionSetting::get();

        $totalQuestions = $request->noofquestions;
        $selectedChapters = $request->chapterIds;
        $selectedChapterTopics = $request->chapterTopicIds;
        $unlock = $request->unlock;
        $pdata = [];
        foreach($settings as $key => $item)
        {

            $pdata[] = $item->percent_selection/100 * $totalQuestions;
            $plevel[] = $item->level_id;
            // $pdata[$item->level_id] = $item->percent_selection/100 * $totalQuestions;
        }
        $nos = $this->func($pdata, $totalQuestions);

        // for($x = 0; $x < count($plevel); $x++)   
        // {
        //     echo $plevel[$x].": ".$nos[$x]."<br>";
        // }

        // random selection

        $a=array("red","green","blue","yellow","brown");
        $locked_ques_ids = [];

        if($unlock == false)
        {

            $lockedQues = TestQuestion::select('question_id')->get();
            
            foreach($lockedQues as $lques)
                $locked_ques_ids[] = $lques->question_id;
            
            $quests = QuestionBank::whereNotIn("id",$locked_ques_ids);
        }
        else if(count($selectedChapters) > 0)
        {
            $quests = QuestionBank::whereIn('chapter_id', $selectedChapters);
        }
        else if(count($selectedChapterTopics) > 0)
        {
            $quests = QuestionBank::whereIn('chapter_topic_id', $selectedChapterTopics);
        }
        else {
            $quests = new QuestionBank();
        }

        $quests = $quests->get();
        $ques_ids = [];
        foreach($quests as $ques)
            $ques_ids[] = $ques->id;

        $random_keys = [];
        for($x = 0; $x < count($plevel); $x++)   
        {
            // echo $plevel[$x].": ".$nos[$x]."<br>";
            $random_keys = array_merge($random_keys, array_rand($ques_ids,$nos[$x]));
        }
        // echo "<pre>"; 
        // print_r($random_keys);
        // exit;
        $questions = QuestionBank::with('subject','chapter','Options')->whereIn("id", $random_keys)->get();
        $content=["keys"=>$random_keys,'data'=>$questions];
        $status = 200;
        $value="application/json";
        return (new Response($content, $status))->header('Content-Type', $value);
    }

    private function func($orig, $target) {

        $i = count($orig);
        $j = 0;
        $total = 0;
        $change=0;
        $newVals = [];
        $next=0;
        $factor1 = 0;
        $factor2 = 0;
        $len = count($orig);
        $marginOfErrors = [];
        
        // map original values to new array
        while ($i--) {
            $total += $newVals[$i] = round($orig[$i]);
        }
        // print_r($newVals);exit;
        
        $change = $total < $target ? 1 : -1;
        while ($total != $target) {

            // Iterate through values and select the one that once changed will introduce
            // the least margin of error in terms of itself. e.g. Incrementing 10 by 1
            // would mean an error of 10% in relation to the value itself.
            for ($i = 0; $i < $len; $i++) {

                $next = $i === $len - 1 ? 0 : $i + 1;

                $factor2 = $this->errorFactor($orig[$next], $newVals[$next] + $change);
                $factor1 = $this->errorFactor($orig[$i], $newVals[$i] + $change);

                if ($factor1 > $factor2) {
                    $j = $next;
                }
            }
            $newVals[$j] += $change;
            $total += $change;
        }
        
        for ($i = 0; $i < $len; $i++) { $marginOfErrors[$i] = $newVals[$i] && abs($orig[$i] - $newVals[$i]) / $orig[$i]; }

        // round() causes some problems as it is difficult to know at the beginning
        // whether numbers should have been rounded up or down to reduce total margin of error. 
        // This section of code increments and decrements values by 1 to find the number
        // combination with least margin of error.
        for ($i = 0; $i < $len; $i++) {
            for ($j = 0; $j < $len; $j++) {
                if ($j === $i) continue;

                $roundUpFactor = $this->errorFactor($orig[$i], $newVals[$i] + 1) + $this->errorFactor($orig[$j], $newVals[$j] - 1);
                $roundDownFactor = $this->errorFactor($orig[$i], $newVals[$i] - 1) + $this->errorFactor($orig[$j], $newVals[$j] + 1);
                $sumMargin = $marginOfErrors[$i] + $marginOfErrors[$j];

                if ($roundUpFactor < $sumMargin) {
                    $newVals[$i] = $newVals[$i] + 1;
                    $newVals[$j] = $newVals[$j] - 1;
                    $marginOfErrors[$i] = $newVals[$i] && abs($orig[$i] - $newVals[$i]) / $orig[$i];
                    $marginOfErrors[$j] = $newVals[$j] && abs($orig[$j] - $newVals[$j]) / $orig[$j];
                }

                if ($roundDownFactor < $sumMargin) {
                    $newVals[$i] = $newVals[$i] - 1;
                    $newVals[$j] = $newVals[$j] + 1;
                    $marginOfErrors[$i] = $newVals[$i] && abs($orig[$i] - $newVals[$i]) / $orig[$i];
                    $marginOfErrors[$j] = $newVals[$j] && abs($orig[$j] - $newVals[$j]) / $orig[$j];
                }

            }
        }
        return $newVals;
    }

    /*private function funcnew($orig, $target) {

        $i = count($orig);
        $j = 0;
        $total = 0;
        $change=0;
        $newVals = [];
        $next=0;
        $factor1 = 0;
        $factor2 = 0;
        $len = count($orig);
        $marginOfErrors = [];
        
        // map original values to new array
        foreach($orig as $key=>$item) {
            $newVals[$key] = round($item);
            $total += $newVals[$key];
            // $total += $newVals[$i] = round($orig[$i]);
        }
        print_r($newVals);exit;
        $change = $total < $target ? 1 : -1;
        while ($total != $target) {

            // Iterate through values and select the one that once changed will introduce
            // the least margin of error in terms of itself. e.g. Incrementing 10 by 1
            // would mean an error of 10% in relation to the value itself.
            for ($i = 0; $i < $len; $i++) {

                $next = $i === $len - 1 ? 0 : $i + 1;

                $factor2 = $this->errorFactor($orig[$next], $newVals[$next] + $change);
                $factor1 = $this->errorFactor($orig[$i], $newVals[$i] + $change);

                if ($factor1 > $factor2) {
                    $j = $next;
                }
            }
            $newVals[$j] += $change;
            $total += $change;
        }
        
        for ($i = 0; $i < $len; $i++) { $marginOfErrors[$i] = $newVals[$i] && abs($orig[$i] - $newVals[$i]) / $orig[$i]; }

        // round() causes some problems as it is difficult to know at the beginning
        // whether numbers should have been rounded up or down to reduce total margin of error. 
        // This section of code increments and decrements values by 1 to find the number
        // combination with least margin of error.
        for ($i = 0; $i < $len; $i++) {
            for ($j = 0; $j < $len; $j++) {
                if ($j === $i) continue;

                $roundUpFactor = $this->errorFactor($orig[$i], $newVals[$i] + 1) + $this->errorFactor($orig[$j], $newVals[$j] - 1);
                $roundDownFactor = $this->errorFactor($orig[$i], $newVals[$i] - 1) + $this->errorFactor($orig[$j], $newVals[$j] + 1);
                $sumMargin = $marginOfErrors[$i] + $marginOfErrors[$j];

                if ($roundUpFactor < $sumMargin) {
                    $newVals[$i] = $newVals[$i] + 1;
                    $newVals[$j] = $newVals[$j] - 1;
                    $marginOfErrors[$i] = $newVals[$i] && abs($orig[$i] - $newVals[$i]) / $orig[$i];
                    $marginOfErrors[$j] = $newVals[$j] && abs($orig[$j] - $newVals[$j]) / $orig[$j];
                }

                if ($roundDownFactor < $sumMargin) {
                    $newVals[$i] = $newVals[$i] - 1;
                    $newVals[$j] = $newVals[$j] + 1;
                    $marginOfErrors[$i] = $newVals[$i] && abs($orig[$i] - $newVals[$i]) / $orig[$i];
                    $marginOfErrors[$j] = $newVals[$j] && abs($orig[$j] - $newVals[$j]) / $orig[$j];
                }

            }
        }
        return $newVals;
    }*/

    public function errorFactor($oldNum, $newNum) {
        return abs($oldNum - $newNum) / $oldNum;
    }

    public function exp()
    {
        return Excel::download(new AppExp, 'invoices.xlsx');
    }
    public function getChaptersData($subject_id)
    {
        
        $chapters = Chapter::withCount('questions')->where("subject_id", $subject_id)->get();
        // $data = ChapterTopic::withCount('questionCount')->where("chapter_id", $chapter_id)->get();
        
        // $chapters = Chapter::where("id", 1)->get();
        // $ques = QuestionBank::where('chapter_id', 1)->get();
        // $totalQuesCount = QuestionBank::where('chapter_id', 1)->count();

        // $lockedQuesCount = TestQuestion::whereIn('question_id',[5,6,10,18,14])->count();
        // $available = $totalQuesCount - $lockedQuesCount;

        // $d = DB::table('question_banks')
        //     ->where('question_banks.chapter_id', 1)
        //     ->join('test_questions', 'question_banks.id', '!=', 'test_questions.question_id')
        //     ->count();
        $mydata=[];
        foreach($chapters as $chapter)
        {
            // dd($chapter);
            $dm = [];
            $query = "SELECT COUNT(*) as available_ques FROM `scholar_question_banks` as `sqb` WHERE sqb.chapter_id = $chapter->id and sqb.id NOT IN (SELECT question_id from scholar_test_questions)";
            $d = DB::select(DB::raw($query));
            
            $dm['id'] = $chapter->id;
            $dm['title'] = $chapter->title;
            $dm['total_questions'] = $chapter->questions_count;
            $dm['available_ques_count'] = $d[0]->available_ques;
            $mydata[]= $dm;
        }
        // dd($mydata);
        $content=['data'=>$mydata];
        $status = 200;
        $value="application/json";
        return (new Response($content, $status))->header('Content-Type', $value);
        

    }

    public function getChapterTopicsData($chapter_id)
    {
        // $chapter = Chapter::withCount('questions')->where("chapter_id", $chapter_id)->get();
        $lockedQuesCount = TestQuestion::select('question_id')->get();
        $topics = ChapterTopic::withCount('questions')->where("chapter_id", $chapter_id)->get();
        $mydata=[];
        foreach($topics as $topic)
        {
            // dd($topic);
            
            $dm = [];
            $query = "SELECT COUNT(*) as available_ques FROM `scholar_question_banks` as `sqb` WHERE sqb.chapter_topic_id = $topic->id and sqb.id NOT IN (SELECT question_id from scholar_test_questions)";
            $d = DB::select(DB::raw($query));
            
            $dm['id'] = $topic->id;
            $dm['title'] = $topic->title;
            $dm['total_questions'] = $topic->questions_count;
            $dm['available_ques_count'] = $d[0]->available_ques;
            $dm['selected'] = false;
            $mydata[]= $dm;
        }
        // dd($mydata);
        $content=['data'=>$mydata];
        $status = 200;
        $value="application/json";
        return (new Response($content, $status))->header('Content-Type', $value);

    }

    public function getTopicQuestionsData($topic_id)
    {
        $mydata = DB::table('question_banks')->where('chapter_topic_id', $topic_id)->whereNotIn('chapter_topic_id', DB::table('test_questions')->select('question_id')->pluck('question_id'))->select(['question_banks.*', 'levels.title as level_title'])->leftjoin('levels','question_banks.level_id', 'levels.id')->get();
        $content=['data'=>$mydata];
        $status = 200;
        $value="application/json";
        return (new Response($content, $status))->header('Content-Type', $value);
    }

}