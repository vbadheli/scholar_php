<?php 
namespace App\Exports;

use App\QuestionBank;
use Maatwebsite\Excel\Concerns\FromCollection;

class AppExp implements FromCollection
{
    public function collection()
    {
        return QuestionBank::all();
    }
}