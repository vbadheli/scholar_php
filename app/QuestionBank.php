<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionBank extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question', 'subject_id', 'chapter_id', 'chapter_topic_id', 'chapter_sub_topic_id', 'level_id'
    ];

    public function subject()
    {
        return $this->belongsTo("\App\Subject");
    }

    public function chapter()
    {
        return $this->belongsTo("\App\Chapter")->with('chapterTopics');
    }
    public function Topic()
    {
        return $this->belongsTo("\App\Chapter");
    }
    public function SubTopic()
    {
        return $this->belongsTo("\App\ChapterSubTopic");
    }

    public function Options()
    {
        return $this->hasMany("\App\QuestionOption","question_id");
    }
}
