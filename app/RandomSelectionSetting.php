<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RandomSelectionSetting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'level_id', 'percent_selection'
    ];
}
