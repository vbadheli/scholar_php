-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 11, 2020 at 05:07 PM
-- Server version: 5.7.31-0ubuntu0.18.04.1
-- PHP Version: 7.2.32-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scholarpanel`
--

-- --------------------------------------------------------

--
-- Table structure for table `scholar_chapters`
--

CREATE TABLE `scholar_chapters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subject_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `scholar_chapters`
--

INSERT INTO `scholar_chapters` (`id`, `subject_id`, `title`, `created_at`, `updated_at`) VALUES
(1, 1, 'Circular motion', NULL, NULL),
(2, 1, 'Gravitation', NULL, NULL),
(3, 1, 'Rotational motio', NULL, NULL),
(4, 1, 'Oscillation', NULL, NULL),
(5, 1, 'Elasticit', NULL, NULL),
(6, 1, 'Surface tensio', NULL, NULL),
(7, 1, 'Wave motio', NULL, NULL),
(8, 1, 'Stationary wave', NULL, NULL),
(9, 1, 'Kinetic theory of gases and Radiatio', NULL, NULL),
(10, 1, 'Wave theory of ligh', NULL, NULL),
(11, 1, 'Interference and diffractio', NULL, NULL),
(12, 1, 'Electrostatics Gauss’', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `scholar_chapter_sub_topics`
--

CREATE TABLE `scholar_chapter_sub_topics` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `topic_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `scholar_chapter_sub_topics`
--

INSERT INTO `scholar_chapter_sub_topics` (`id`, `topic_id`, `title`, `created_at`, `updated_at`) VALUES
(1, 25, 'nulla integer pede', '2019-10-11 02:32:56', '2020-05-21 18:37:48'),
(2, 49, 'mauris vulputate elementum', '2020-03-02 07:50:46', '2020-04-10 03:25:34'),
(3, 2, 'venenatis turpis', '2019-10-05 10:12:19', '2019-09-30 22:40:40'),
(4, 1, 'tortor sollicitudin mi sit', '2020-05-08 22:31:54', '2019-11-15 11:14:14'),
(5, 48, 'curabitur in libero ut massa', '2020-04-28 19:45:29', '2020-02-08 21:47:17'),
(6, 27, 'varius integer ac leo pellentesque', '2020-02-03 01:03:22', '2020-06-17 22:28:45'),
(7, 33, 'amet erat nulla tempus vivamus', '2020-03-26 05:50:09', '2019-10-16 09:55:49'),
(8, 1, 'curabitur at', '2019-10-08 06:41:19', '2020-01-22 07:28:37'),
(9, 31, 'tristique in', '2019-10-20 22:08:41', '2020-08-05 18:51:05'),
(10, 43, 'leo odio porttitor', '2020-02-06 11:09:28', '2019-10-21 04:43:46'),
(11, 19, 'ipsum primis in faucibus', '2020-02-06 22:56:46', '2020-04-07 08:51:53'),
(12, 18, 'nulla dapibus dolor', '2020-06-25 13:43:05', '2020-03-18 22:08:06'),
(13, 13, 'nulla mollis molestie lorem quisque', '2020-06-21 05:26:47', '2020-03-15 20:05:14'),
(14, 9, 'cubilia curae duis', '2019-09-08 14:11:35', '2019-09-15 01:25:12'),
(15, 40, 'blandit nam', '2020-07-03 04:16:31', '2020-05-03 20:03:28'),
(16, 34, 'vestibulum ante ipsum', '2019-10-12 06:50:16', '2020-04-01 06:54:42'),
(17, 37, 'tortor sollicitudin mi', '2020-07-24 04:21:11', '2020-03-10 22:37:32'),
(18, 44, 'platea dictumst etiam faucibus cursus', '2020-04-20 03:40:57', '2020-06-10 02:08:50'),
(19, 20, 'vestibulum ante ipsum primis', '2019-11-01 12:44:07', '2020-03-19 16:50:52'),
(20, 11, 'blandit mi', '2020-05-29 16:28:14', '2020-06-27 13:45:56'),
(21, 8, 'blandit nam nulla integer pede', '2019-11-07 22:58:38', '2019-08-07 16:40:08'),
(22, 18, 'elementum pellentesque', '2019-10-07 14:17:45', '2020-05-29 17:07:40'),
(23, 45, 'ut tellus nulla ut', '2020-02-24 18:06:52', '2020-07-20 17:13:22'),
(24, 21, 'cras non velit', '2020-04-27 04:53:10', '2019-09-17 21:50:12'),
(25, 50, 'lobortis est phasellus', '2020-06-17 19:58:49', '2020-04-03 13:24:34'),
(26, 1, 'tempus vel', '2020-03-20 11:26:18', '2019-09-17 20:11:10'),
(27, 16, 'curae duis faucibus', '2019-12-23 10:22:36', '2019-11-09 14:06:38'),
(28, 42, 'ac est lacinia', '2019-11-11 14:01:18', '2019-10-24 21:32:04'),
(29, 10, 'sed tincidunt eu felis fusce', '2019-12-20 15:38:12', '2020-04-09 19:40:46'),
(30, 42, 'nam ultrices libero', '2019-08-11 17:36:03', '2020-02-27 12:56:13'),
(31, 2, 'vitae nisi', '2019-10-04 16:16:15', '2020-07-11 06:56:33'),
(32, 20, 'nisl ut volutpat', '2020-03-04 04:48:44', '2019-12-30 23:16:13'),
(33, 50, 'nunc donec quis orci', '2020-07-05 17:48:00', '2020-05-15 22:09:19'),
(34, 3, 'cursus urna ut', '2020-02-06 22:56:34', '2020-02-12 00:33:08'),
(35, 36, 'magna vestibulum aliquet', '2020-03-01 17:13:53', '2020-03-15 10:48:51'),
(36, 38, 'elit proin interdum mauris', '2019-09-02 23:15:48', '2020-04-22 11:56:28'),
(37, 34, 'lorem quisque ut erat', '2020-07-12 16:36:34', '2020-01-18 23:12:42'),
(38, 25, 'orci vehicula condimentum curabitur in', '2020-04-23 22:37:01', '2020-01-26 12:33:13'),
(39, 3, 'aliquam non mauris morbi', '2020-01-06 23:11:56', '2020-03-09 22:20:46'),
(40, 22, 'ipsum ac', '2020-04-18 00:43:24', '2019-12-09 05:47:19'),
(41, 10, 'ultrices aliquet maecenas leo odio', '2020-02-06 17:55:24', '2020-06-14 19:24:53'),
(42, 27, 'sapien urna pretium nisl ut', '2019-12-08 21:51:02', '2020-02-06 00:58:52'),
(43, 27, 'penatibus et', '2020-03-28 12:35:49', '2020-03-28 03:03:54'),
(44, 36, 'convallis morbi odio odio', '2020-03-14 12:17:12', '2020-05-27 10:30:28'),
(45, 12, 'habitasse platea dictumst aliquam augue', '2019-10-23 05:15:04', '2020-05-04 14:43:56'),
(46, 4, 'vel ipsum praesent blandit lacinia', '2019-10-19 19:04:22', '2020-02-06 08:22:06'),
(47, 40, 'curae mauris viverra', '2020-01-25 06:33:10', '2020-01-20 02:53:06'),
(48, 35, 'semper sapien a libero nam', '2020-02-18 20:18:48', '2020-01-26 05:02:04'),
(49, 12, 'pellentesque eget', '2019-09-13 15:14:03', '2019-11-29 19:12:14'),
(50, 23, 'quis augue luctus', '2019-10-10 15:36:16', '2020-01-10 11:36:47'),
(51, 38, 'blandit mi in porttitor pede', '2019-09-07 23:59:36', '2019-10-11 17:54:08'),
(52, 15, 'vivamus metus arcu', '2020-05-03 20:27:27', '2020-06-18 11:04:07'),
(53, 17, 'ultrices aliquet maecenas leo odio', '2019-12-25 22:46:37', '2019-09-18 22:09:57'),
(54, 6, 'non lectus aliquam sit', '2019-12-28 19:35:35', '2020-05-13 06:19:43'),
(55, 22, 'pulvinar nulla pede', '2020-05-22 06:59:06', '2019-12-29 01:06:30'),
(56, 24, 'vestibulum quam sapien varius', '2020-07-07 18:20:09', '2019-08-15 00:44:52'),
(57, 35, 'nulla nisl nunc', '2020-07-13 14:44:37', '2020-01-31 17:09:19'),
(58, 21, 'blandit ultrices', '2020-03-11 10:04:20', '2020-04-04 18:09:15'),
(59, 31, 'porttitor lorem id', '2020-06-17 04:00:37', '2020-01-05 03:59:21'),
(60, 35, 'diam id', '2019-12-11 16:16:10', '2019-08-13 10:33:24'),
(61, 43, 'lobortis vel dapibus', '2019-10-24 16:17:23', '2019-10-13 13:55:44'),
(62, 48, 'pulvinar nulla pede ullamcorper', '2020-07-04 19:55:39', '2019-10-23 12:30:00'),
(63, 6, 'arcu sed augue', '2020-07-26 03:50:27', '2020-04-27 22:20:09'),
(64, 35, 'amet erat nulla', '2020-02-12 08:20:03', '2020-05-12 13:34:05'),
(65, 20, 'in felis', '2020-02-01 23:16:07', '2020-05-17 13:41:04'),
(66, 9, 'justo sollicitudin ut suscipit', '2019-08-18 00:05:45', '2019-12-26 15:00:50'),
(67, 18, 'nisi eu orci mauris lacinia', '2019-10-09 01:54:37', '2020-02-26 12:22:26'),
(68, 21, 'vestibulum vestibulum ante', '2019-12-18 00:15:57', '2020-02-22 02:12:17'),
(69, 10, 'amet eleifend pede libero', '2020-06-01 13:28:08', '2020-06-18 17:32:27'),
(70, 30, 'pede venenatis non sodales sed', '2020-06-01 04:06:56', '2019-08-16 00:01:36'),
(71, 32, 'molestie hendrerit', '2020-07-29 17:14:33', '2019-12-21 16:54:17'),
(72, 34, 'placerat praesent blandit nam nulla', '2020-04-08 23:34:01', '2019-12-25 21:00:39'),
(73, 46, 'libero non mattis pulvinar', '2019-10-18 15:28:12', '2020-04-28 00:38:25'),
(74, 21, 'volutpat dui maecenas tristique est', '2019-10-16 03:37:48', '2019-09-06 02:30:58'),
(75, 20, 'in faucibus', '2019-11-29 07:01:42', '2019-08-22 05:51:19'),
(76, 25, 'phasellus sit amet erat', '2020-07-11 13:09:06', '2020-06-17 23:57:29'),
(77, 14, 'cubilia curae donec', '2020-07-10 01:37:30', '2020-03-09 15:30:03'),
(78, 5, 'elementum nullam varius', '2020-07-03 11:08:28', '2020-03-07 13:07:15'),
(79, 3, 'eu sapien', '2020-07-08 06:07:26', '2020-05-26 16:40:20'),
(80, 41, 'eu magna vulputate luctus cum', '2020-03-19 10:19:55', '2020-07-27 05:46:11'),
(81, 16, 'mauris viverra', '2020-05-29 08:24:04', '2019-10-17 20:39:03'),
(82, 2, 'eu sapien cursus vestibulum proin', '2019-09-24 09:57:32', '2020-05-22 15:09:30'),
(83, 31, 'magna bibendum imperdiet nullam orci', '2019-09-25 08:50:04', '2019-10-19 02:52:11'),
(84, 7, 'pharetra magna vestibulum aliquet ultrices', '2019-12-06 17:41:35', '2019-09-15 14:17:05'),
(85, 9, 'vestibulum vestibulum', '2019-12-07 10:12:06', '2019-12-07 04:05:06'),
(86, 1, 'curabitur convallis duis consequat', '2019-09-24 06:46:29', '2019-10-14 14:34:29'),
(87, 10, 'quisque arcu libero rutrum', '2020-04-22 20:21:39', '2019-11-20 23:35:25'),
(88, 27, 'ullamcorper augue a suscipit', '2019-10-09 19:18:19', '2020-01-19 20:04:37'),
(89, 30, 'nibh in hac', '2020-02-07 21:43:16', '2019-08-26 16:46:37'),
(90, 48, 'nibh fusce lacus purus', '2019-11-05 12:03:54', '2020-02-10 04:26:55'),
(91, 38, 'mauris non', '2020-02-17 03:28:40', '2019-08-28 11:54:20'),
(92, 27, 'in felis donec semper sapien', '2019-10-16 07:24:13', '2020-03-04 18:50:58'),
(93, 18, 'mauris eget massa tempor convallis', '2019-09-08 21:57:14', '2020-03-13 02:07:27'),
(94, 47, 'vulputate justo', '2020-06-05 01:48:07', '2020-04-25 00:20:59'),
(95, 22, 'odio justo sollicitudin ut', '2019-10-01 06:37:48', '2020-01-16 03:26:29'),
(96, 29, 'vestibulum vestibulum', '2019-09-05 07:36:13', '2019-10-18 07:54:32'),
(97, 48, 'ultrices mattis odio donec vitae', '2019-08-28 01:14:20', '2019-11-16 14:39:26'),
(98, 7, 'tristique in tempus sit', '2019-10-09 02:53:00', '2019-09-15 04:30:39'),
(99, 48, 'duis bibendum', '2019-09-02 18:28:32', '2020-07-19 10:03:32'),
(100, 45, 'ac diam cras pellentesque', '2020-02-20 08:03:52', '2020-05-02 14:16:19'),
(101, 49, 'sed nisl nunc rhoncus dui', '2019-09-24 05:43:54', '2019-09-26 02:59:21'),
(102, 44, 'eleifend quam a', '2020-07-05 15:10:32', '2019-12-28 06:05:18'),
(103, 34, 'eget massa', '2020-06-18 00:42:31', '2020-08-06 12:39:54'),
(104, 40, 'felis eu', '2019-10-10 23:17:05', '2020-05-31 12:05:32'),
(105, 48, 'curabitur convallis duis consequat', '2019-10-13 09:21:30', '2020-06-13 20:35:55'),
(106, 13, 'nulla eget eros elementum pellentesque', '2020-07-03 21:37:48', '2019-10-08 04:23:39'),
(107, 41, 'nisl aenean lectus pellentesque', '2019-12-02 04:06:33', '2020-07-24 22:36:46'),
(108, 41, 'orci eget orci vehicula', '2020-04-14 05:48:12', '2020-04-07 01:24:19'),
(109, 5, 'platea dictumst maecenas', '2020-04-13 19:41:37', '2020-02-09 09:03:56'),
(110, 18, 'rhoncus aliquet pulvinar sed nisl', '2020-07-08 07:36:51', '2019-11-14 10:50:45'),
(111, 38, 'blandit lacinia erat vestibulum sed', '2020-02-20 08:40:02', '2019-08-12 16:55:41'),
(112, 28, 'suscipit a', '2020-03-11 23:09:18', '2020-07-09 03:20:52'),
(113, 28, 'mi integer ac neque', '2019-08-09 01:58:41', '2020-03-06 21:48:14'),
(114, 32, 'ipsum primis', '2020-06-01 10:42:32', '2019-09-28 23:25:04'),
(115, 6, 'viverra dapibus nulla suscipit', '2019-11-21 18:50:46', '2020-07-09 21:11:56'),
(116, 15, 'nisi volutpat', '2019-10-11 17:40:52', '2020-04-11 13:50:19'),
(117, 47, 'lobortis vel dapibus', '2019-12-19 17:33:26', '2020-02-24 23:35:59'),
(118, 7, 'sodales scelerisque mauris sit', '2020-03-31 10:02:34', '2019-12-07 05:29:30'),
(119, 16, 'lacus morbi', '2019-12-30 22:39:51', '2020-04-20 00:52:53'),
(120, 9, 'pharetra magna ac consequat', '2020-05-04 16:27:08', '2019-08-31 19:26:32'),
(121, 48, 'nullam porttitor lacus at', '2020-07-08 03:52:07', '2020-03-22 14:10:23'),
(122, 18, 'mi sit amet lobortis', '2020-02-22 07:40:33', '2020-05-14 05:13:08'),
(123, 15, 'sapien iaculis', '2019-11-08 02:14:10', '2020-03-04 18:04:29'),
(124, 41, 'cum sociis natoque', '2019-10-28 03:59:30', '2020-02-22 15:35:22'),
(125, 6, 'duis faucibus accumsan', '2019-10-19 03:46:25', '2020-01-14 04:10:43'),
(126, 23, 'commodo vulputate justo', '2020-06-06 07:34:19', '2019-08-18 15:05:54'),
(127, 32, 'non velit', '2020-03-26 00:04:05', '2020-01-14 10:38:09'),
(128, 31, 'magna vestibulum aliquet ultrices', '2020-02-05 02:58:56', '2020-03-11 00:47:36'),
(129, 25, 'integer non', '2019-10-28 04:46:26', '2019-08-07 11:06:27'),
(130, 13, 'sapien dignissim vestibulum vestibulum', '2020-04-14 04:36:06', '2019-10-10 15:11:34'),
(131, 24, 'rutrum at lorem', '2020-02-03 04:52:05', '2020-05-02 04:20:55'),
(132, 4, 'sodales sed', '2019-09-25 10:39:44', '2020-04-12 07:52:43'),
(133, 42, 'aliquam lacus morbi quis', '2020-02-21 23:04:44', '2019-08-24 19:03:12'),
(134, 23, 'cursus urna ut', '2020-07-24 14:08:54', '2020-01-10 01:16:16'),
(135, 39, 'nullam molestie nibh', '2019-09-22 02:00:47', '2019-10-22 19:26:04'),
(136, 4, 'tempus semper est quam', '2020-06-01 15:35:24', '2020-01-07 09:53:49'),
(137, 47, 'vulputate luctus cum sociis', '2020-05-12 17:18:49', '2020-08-03 15:30:09'),
(138, 38, 'in faucibus orci luctus', '2019-11-30 12:41:35', '2020-06-08 21:50:48'),
(139, 14, 'a suscipit', '2020-04-24 06:09:47', '2019-10-02 14:48:07'),
(140, 9, 'aliquet ultrices', '2020-04-29 09:39:50', '2020-02-05 21:00:34'),
(141, 2, 'sed tristique in tempus sit', '2019-12-14 18:04:44', '2019-12-04 06:34:13'),
(142, 49, 'fringilla rhoncus mauris enim leo', '2020-07-19 08:14:00', '2020-01-28 06:37:43'),
(143, 24, 'vestibulum velit id pretium', '2019-08-21 09:41:25', '2020-01-08 03:41:51'),
(144, 18, 'gravida nisi at nibh in', '2020-01-27 16:48:58', '2020-03-31 11:58:53'),
(145, 15, 'est phasellus sit', '2020-06-29 08:45:50', '2020-08-03 17:05:21'),
(146, 27, 'bibendum imperdiet', '2019-11-08 11:51:04', '2019-12-20 00:41:44'),
(147, 25, 'at nunc commodo placerat', '2020-03-11 23:37:32', '2020-05-27 13:48:30'),
(148, 2, 'mauris morbi', '2020-01-25 14:34:10', '2020-05-28 18:03:44'),
(149, 7, 'in tempor turpis nec euismod', '2020-03-15 07:46:42', '2020-04-15 06:56:37'),
(150, 6, 'at feugiat non', '2020-02-25 00:05:31', '2019-10-29 18:59:12');

-- --------------------------------------------------------

--
-- Table structure for table `scholar_chapter_topics`
--

CREATE TABLE `scholar_chapter_topics` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `scholar_chapter_topics`
--

INSERT INTO `scholar_chapter_topics` (`id`, `chapter_id`, `title`, `created_at`, `updated_at`) VALUES
(1, 1, 'Angular displacement', NULL, NULL),
(2, 1, 'Angular velocity and angular acceleration', NULL, NULL),
(3, 1, 'Relation between linear velocity and angular velocity', NULL, NULL),
(4, 1, 'Uniform circular motion', NULL, NULL),
(5, 1, 'Radial acceleration', NULL, NULL),
(6, 1, 'Centripetal and centrifugal forces', NULL, NULL),
(7, 1, 'Banking of roads', NULL, NULL),
(8, 1, 'Vertical circular motion due to earth’s gravitation', NULL, NULL),
(9, 1, 'Equation for velocity and energy at different positions of vertical circular motion. Kinematical equations for circular motion in analogy with linear motion.', NULL, NULL),
(11, 2, 'Newton’s law of gravitation', NULL, NULL),
(12, 2, 'Projection of satellite', NULL, NULL),
(13, 2, 'Periodic time', NULL, NULL),
(14, 2, 'Statement of Kepler’s laws of motion', NULL, NULL),
(15, 2, 'Binding energy and escape velocity of a satellite', NULL, NULL),
(16, 2, 'Weightlessness condition in orbit', NULL, NULL),
(17, 2, 'Variation of ‘g’ due to altitude', NULL, NULL),
(18, 2, 'lattitude', NULL, NULL),
(19, 2, 'depth and motion', NULL, NULL),
(20, 2, 'Communication satellite and its uses.', NULL, NULL),
(21, 3, 'Definition of M.I.', NULL, NULL),
(22, 3, 'K.E. of rotating body', NULL, NULL),
(23, 3, 'Rolling motion', NULL, NULL),
(24, 3, 'Physical significance of M.I.', NULL, NULL),
(25, 3, 'Radius of gyration', NULL, NULL),
(26, 3, 'Torque', NULL, NULL),
(27, 3, 'Principle of parallel and perpendicular axes', NULL, NULL),
(28, 3, 'M.I. of some regular shaped bodies about specific axes', NULL, NULL),
(29, 3, 'Angular momentum and its conservation.', NULL, NULL),
(30, 4, 'Explanation of periodic motion', NULL, NULL),
(31, 4, 'S.H.M.', NULL, NULL),
(32, 4, 'Differential equation of linear S.H.M. Projection of U.C.M. on any diameter', NULL, NULL),
(33, 4, 'Phase of S.H.M.', NULL, NULL),
(34, 4, 'K.E. and P.E. in S.H.M.', NULL, NULL),
(35, 4, 'Composition of two S.H.M.’s having same period and along same line', NULL, NULL),
(36, 4, 'Simple pendulum', NULL, NULL),
(37, 4, 'Damped S.H.M.', NULL, NULL),
(38, 5, 'General explanation of elastic property', NULL, NULL),
(39, 5, 'Plasticity', NULL, NULL),
(40, 5, 'Deformation', NULL, NULL),
(41, 5, 'Definition of stress and strain', NULL, NULL),
(42, 5, 'Hooke’s law', NULL, NULL),
(43, 5, 'Poisson’s ratio', NULL, NULL),
(44, 5, 'Elastic energy', NULL, NULL),
(45, 5, 'Elastic constants and their relation', NULL, NULL),
(46, 5, 'Determination of ‘Y’', NULL, NULL),
(47, 5, 'Behaviour of metal wire under increasing load', NULL, NULL),
(48, 5, 'Applications of elastic behaviour of materials.', NULL, NULL),
(49, 6, 'Surface tension on the basis of molecular theory', NULL, NULL),
(50, 6, 'Surface tension', NULL, NULL),
(51, 6, 'Capillarity and capillary action', NULL, NULL),
(52, 6, 'Simple harmonic progressive waves', NULL, NULL),
(53, 6, 'Change of phase', NULL, NULL),
(54, 6, 'Formation of beats', NULL, NULL),
(55, 6, 'Formation of stationary waves on string', NULL, NULL),
(56, 6, 'Reflection of transverse and longitudinal waves', NULL, NULL),
(57, 6, 'Change of phase', NULL, NULL),
(58, 6, 'Superposition of waves', NULL, NULL),
(59, 6, 'Formation of beats', NULL, NULL),
(60, 6, 'Doppler effect in sound. 8. Stationary waves Study of vibrations in a finite medium', NULL, NULL),
(61, 6, 'Formation of stationary waves on string', NULL, NULL),
(62, 6, 'Study of vibrations of air columns', NULL, NULL),
(63, 6, 'Free and Forced vibrations', NULL, NULL),
(64, 6, 'Resonance.', NULL, NULL),
(65, 6, 'Study of vibrations in a finite medium', NULL, NULL),
(66, 6, 'Formation of stationary waves on string', NULL, NULL),
(67, 6, 'Study of vibrations of air columns', NULL, NULL),
(68, 6, 'Free and Forced vibrations', NULL, NULL),
(69, 6, 'Resonance.', NULL, NULL),
(70, 6, 'Concept of an ideal gas', NULL, NULL),
(71, 6, 'Assumptions of kinetic theory', NULL, NULL),
(72, 6, 'Mean free path', NULL, NULL),
(73, 7, 'Derivation for pressure of a gas', NULL, NULL),
(74, 7, 'Degrees of freedom', NULL, NULL),
(75, 7, 'Derivation of Boyle’s law', NULL, NULL),
(76, 7, 'Thermodynamics- Thermal equilibrium and definition of temperature', NULL, NULL),
(77, 7, '1st law of thermodynamics', NULL, NULL),
(78, 7, '2nd law of thermodynamics', NULL, NULL),
(79, 7, 'Heat engines and refrigerators', NULL, NULL),
(80, 7, 'Qualitative idea of black body radiation', NULL, NULL),
(81, 7, 'Wein’s displacement law', NULL, NULL),
(82, 7, 'Green house effect', NULL, NULL),
(83, 7, 'Stefan’s law', NULL, NULL),
(84, 7, 'Maxwell distribution', NULL, NULL),
(85, 7, 'Law of equipartition of energy and application to Specific heat capacities of gases.', NULL, NULL),
(86, 7, 'Wave theory of light', NULL, NULL),
(87, 7, 'Huygens’ Principle', NULL, NULL),
(88, 7, 'Construction of plane and spherical wave front', NULL, NULL),
(89, 7, 'Wave front and wave normal', NULL, NULL),
(90, 7, 'Reflection at plane surface', NULL, NULL),
(91, 7, 'Refraction at plane surface', NULL, NULL),
(92, 7, 'Polarisation', NULL, NULL),
(93, 8, 'Polaroids', NULL, NULL),
(94, 8, 'Plane polarised light', NULL, NULL),
(95, 8, 'Brewster’s law', NULL, NULL),
(96, 8, 'Doppler effect in light.', NULL, NULL),
(97, 8, 'Interference of light', NULL, NULL),
(98, 8, 'Conditions for producing steady interference pattern', NULL, NULL),
(99, 8, 'Young’s experiment', NULL, NULL),
(100, 8, 'Analytical treatment of interference bands', NULL, NULL),
(101, 8, 'Measurement of wavelength by biprism experiment', NULL, NULL),
(102, 8, 'Diffraction due to single slit', NULL, NULL),
(103, 8, 'Rayleigh’s criterion', NULL, NULL),
(104, 8, 'Resolving power of a microscope and telescope', NULL, NULL),
(105, 8, 'Difference between interference and diffraction.', NULL, NULL),
(106, 8, 'Theorem proof and applications', NULL, NULL),
(107, 8, 'Mechanical force on unit area of a charged conductor', NULL, NULL),
(108, 8, 'Energy density of a medium', NULL, NULL),
(109, 8, 'Dielectrics and electric polarisation', NULL, NULL),
(110, 8, 'Concept of condenser', NULL, NULL),
(111, 8, 'Capacity of parallel plate condenser', NULL, NULL),
(112, 8, 'Effect of dielectric on capacity', NULL, NULL),
(113, 9, 'Energy of charged condenser', NULL, NULL),
(114, 9, 'Condensers in series and parallel', NULL, NULL),
(115, 9, 'van-deGraaff generator.', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `scholar_levels`
--

CREATE TABLE `scholar_levels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `scholar_levels`
--

INSERT INTO `scholar_levels` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Hard', NULL, NULL),
(2, 'Medium', NULL, NULL),
(3, 'Easy', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `scholar_migrations`
--

CREATE TABLE `scholar_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `scholar_migrations`
--

INSERT INTO `scholar_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_07_18_234201_create_subjects_table', 1),
(2, '2020_07_18_234213_create_types_table', 1),
(3, '2020_07_18_234221_create_levels_table', 1),
(4, '2020_07_18_234252_create__question_banks_table', 1),
(5, '2020_07_18_234305_create__question_options_table', 1),
(6, '2020_07_18_234317_create__question_images_table', 1),
(7, '2020_07_18_234325_create__question_option_images_table', 1),
(8, '2020_07_18_234339_create__question_solutions_table', 1),
(9, '2020_07_18_234352_create__test_question_papers_table', 1),
(10, '2020_07_18_234402_create__test_questions_table', 1),
(11, '2020_07_18_234901_create_chapters_table', 1),
(12, '2020_07_18_234941_create__chapter_topics_table', 1),
(13, '2020_07_18_235007_create__chapter_sub_topics_table', 1),
(14, '2020_08_01_095226_create_random_selection_settings_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `scholar_question_banks`
--

CREATE TABLE `scholar_question_banks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `level_id` int(11) NOT NULL,
  `answer` int(11) DEFAULT NULL,
  `subject_id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `chapter_topic_id` int(11) NOT NULL,
  `chapter_sub_topic_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `scholar_question_banks`
--

INSERT INTO `scholar_question_banks` (`id`, `question`, `level_id`, `answer`, `subject_id`, `chapter_id`, `chapter_topic_id`, `chapter_sub_topic_id`, `created_at`, `updated_at`) VALUES
(1, 'Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis.', 2, 18, 1, 9, 81, 12, '2019-09-01 02:31:36', '2019-09-11 22:18:35'),
(2, 'Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque.', 3, 16, 1, 12, 21, 43, '2020-02-11 02:39:20', '2020-04-09 23:43:09'),
(3, 'Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.', 3, 48, 1, 11, 65, 87, '2020-07-03 09:45:55', '2019-11-12 04:29:36'),
(4, 'Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.', 1, 92, 1, 10, 31, 44, '2019-08-07 17:17:10', '2020-03-02 07:38:55'),
(5, 'Proin at turpis a pede posuere nonummy.', 2, 45, 1, 1, 34, 80, '2019-11-19 01:37:55', '2019-12-31 17:13:14'),
(6, 'Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit.', 3, 68, 1, 1, 96, 62, '2019-10-31 08:53:50', '2020-05-02 09:37:08'),
(7, 'Sed ante.', 3, 27, 1, 2, 96, 78, '2020-01-20 12:35:45', '2020-02-11 19:36:25'),
(8, 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim.', 2, 13, 1, 6, 10, 65, '2019-12-29 12:59:13', '2020-03-17 17:39:48'),
(9, 'Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum.', 2, 11, 1, 3, 51, 2, '2020-07-25 23:11:12', '2020-02-29 11:24:28'),
(10, 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci.', 2, 89, 1, 1, 1, 92, '2019-08-21 19:43:07', '2020-04-20 21:36:54'),
(11, 'Mauris ullamcorper purus sit amet nulla.', 1, 74, 1, 11, 60, 42, '2020-01-31 19:40:48', '2019-12-30 19:16:59'),
(12, 'Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor.', 1, 26, 1, 8, 55, 33, '2020-02-23 15:42:05', '2020-04-04 09:01:13'),
(13, 'Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis.', 1, 63, 1, 8, 10, 8, '2020-03-25 14:25:03', '2019-08-14 16:14:07'),
(14, 'Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum.', 1, 15, 1, 6, 88, 52, '2019-10-30 19:37:45', '2020-04-18 10:28:36'),
(15, 'Aliquam sit amet diam in magna bibendum imperdiet.', 2, 7, 1, 12, 56, 2, '2020-02-24 20:22:18', '2020-05-10 14:33:06'),
(16, 'Proin risus.', 1, 100, 1, 9, 1, 100, '2019-10-12 00:50:14', '2019-12-15 21:43:17'),
(17, 'Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 3, 16, 1, 12, 9, 76, '2020-05-31 00:14:23', '2020-02-26 23:39:54'),
(18, 'Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', 2, 43, 1, 1, 38, 61, '2019-10-06 14:54:21', '2020-03-17 13:24:38'),
(19, 'Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui.', 3, 9, 1, 11, 58, 100, '2020-02-19 17:28:22', '2019-08-30 07:24:01'),
(20, 'Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum.', 1, 86, 1, 9, 4, 33, '2020-06-19 00:39:08', '2019-11-02 12:50:09'),
(21, 'Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo.', 1, 30, 1, 6, 38, 29, '2019-11-17 21:09:17', '2020-05-07 12:16:05'),
(22, 'Donec semper sapien a libero.', 2, 42, 1, 12, 58, 42, '2020-06-15 09:10:54', '2019-12-07 13:39:00'),
(23, 'Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio.', 1, 91, 1, 11, 1, 59, '2020-04-22 23:44:34', '2020-07-11 02:43:46'),
(24, 'Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi.', 1, 32, 1, 5, 6, 92, '2019-11-24 10:14:03', '2020-05-28 17:16:03'),
(25, 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 2, 48, 1, 6, 38, 12, '2020-01-17 07:06:30', '2020-02-20 00:55:15'),
(26, 'Phasellus sit amet erat. Nulla tempus.', 1, 69, 1, 5, 81, 85, '2019-12-28 01:52:34', '2020-01-08 21:52:35'),
(27, 'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus.', 1, 70, 1, 5, 96, 73, '2020-06-09 11:48:43', '2020-01-02 07:57:49'),
(28, 'Proin risus. Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci.', 3, 70, 1, 10, 10, 60, '2019-08-12 23:35:27', '2019-10-01 03:36:50'),
(29, 'Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum.', 3, 88, 1, 11, 100, 76, '2019-11-05 08:48:09', '2020-03-21 07:58:34'),
(30, 'Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', 3, 4, 1, 5, 85, 36, '2019-08-29 16:27:05', '2020-04-08 06:30:02'),
(31, 'Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat.', 1, 33, 1, 7, 1, 15, '2020-04-29 03:39:19', '2020-07-12 09:38:32'),
(32, 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam.', 1, 57, 1, 5, 1, 33, '2019-12-27 12:46:11', '2020-03-28 17:24:58'),
(33, 'Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.', 3, 88, 1, 11, 94, 25, '2020-06-01 09:13:47', '2020-01-21 04:32:26'),
(34, 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui.', 1, 22, 1, 3, 32, 70, '2020-01-10 22:59:34', '2020-06-20 23:47:44'),
(35, 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.', 1, 59, 1, 8, 41, 29, '2019-10-14 21:24:19', '2019-12-05 12:36:38'),
(36, 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis.', 1, 76, 1, 3, 63, 80, '2020-06-09 17:01:21', '2019-12-10 01:01:09'),
(37, 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci.', 2, 90, 1, 11, 65, 44, '2019-12-17 13:31:52', '2019-09-19 02:09:07'),
(38, 'Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc.', 2, 8, 1, 5, 55, 1, '2019-11-23 04:54:52', '2020-05-24 03:48:40'),
(39, 'In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus.', 3, 15, 1, 11, 30, 85, '2020-04-27 10:46:15', '2020-06-18 06:10:14'),
(40, 'Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 2, 58, 1, 7, 100, 55, '2019-09-01 11:17:10', '2020-07-31 20:23:51'),
(41, 'Duis at velit eu est congue elementum. In hac habitasse platea dictumst.', 2, 48, 1, 8, 77, 59, '2020-02-07 13:35:50', '2020-03-31 17:54:32'),
(42, 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 2, 75, 1, 2, 1, 69, '2020-04-14 21:36:10', '2019-10-06 07:19:21'),
(43, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', 2, 76, 1, 2, 38, 68, '2019-11-22 20:24:17', '2019-11-27 00:36:07'),
(44, 'Duis at velit eu est congue elementum. In hac habitasse platea dictumst.', 3, 32, 1, 1, 74, 71, '2020-03-27 15:20:34', '2020-01-02 03:12:24'),
(45, 'Nulla justo.', 1, 43, 1, 2, 68, 8, '2019-10-24 19:09:40', '2020-08-03 05:01:59'),
(46, 'Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus.', 2, 7, 1, 3, 95, 71, '2019-09-22 01:37:37', '2020-03-13 18:10:09'),
(47, 'Vestibulum sed magna at nunc commodo placerat. Praesent blandit. Nam nulla. Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl.', 3, 55, 1, 7, 54, 84, '2019-11-01 05:06:06', '2020-03-20 10:29:48'),
(48, 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', 1, 70, 1, 12, 44, 61, '2019-09-23 15:40:57', '2020-03-28 04:11:51'),
(49, 'Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum. Mauris ullamcorper purus sit amet nulla. Quisque arcu libero, rutrum ac, lobortis vel, dapibus at, diam.', 1, 83, 1, 6, 34, 86, '2019-10-20 03:27:50', '2020-01-24 12:28:55'),
(50, 'Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 2, 20, 1, 9, 35, 71, '2020-03-04 15:22:53', '2020-03-02 02:42:55'),
(51, 'Donec vitae nisi.', 2, 37, 1, 6, 78, 71, '2020-06-17 14:49:50', '2019-10-23 16:55:54'),
(52, 'In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt.', 3, 22, 1, 10, 5, 55, '2019-12-17 21:46:54', '2020-03-09 20:31:55'),
(53, 'Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor.', 1, 26, 1, 9, 79, 9, '2019-11-29 12:42:22', '2019-12-13 16:47:46'),
(54, 'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum.', 3, 61, 1, 12, 43, 62, '2020-01-14 17:10:41', '2019-09-28 23:57:20'),
(55, 'Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', 2, 51, 1, 12, 50, 85, '2020-05-01 03:57:30', '2020-06-11 07:00:41'),
(56, 'Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue.', 1, 61, 1, 10, 100, 81, '2020-04-29 18:30:24', '2019-09-14 06:34:56'),
(57, 'In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum.', 2, 64, 1, 2, 85, 88, '2019-11-09 11:20:21', '2019-12-12 07:41:53'),
(58, 'Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero.', 1, 86, 1, 3, 55, 69, '2020-03-21 05:28:21', '2020-01-04 15:19:31'),
(59, 'Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo.', 3, 29, 1, 1, 96, 60, '2020-07-27 04:36:13', '2019-10-24 02:29:11'),
(60, 'Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet.', 2, 15, 1, 1, 79, 22, '2019-10-13 03:34:42', '2020-07-12 12:01:20'),
(61, 'Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.', 3, 46, 1, 12, 59, 96, '2020-02-19 07:03:04', '2019-09-30 04:50:23'),
(62, 'Donec quis orci eget orci vehicula condimentum.', 2, 84, 1, 4, 27, 4, '2019-10-19 17:38:42', '2020-07-25 05:42:16'),
(63, 'Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', 3, 95, 1, 4, 48, 87, '2020-01-01 23:13:38', '2019-09-16 16:54:20'),
(64, 'Ut at dolor quis odio consequat varius.', 1, 88, 1, 12, 22, 18, '2020-06-25 21:58:42', '2020-04-23 21:50:35'),
(65, 'Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci.', 2, 26, 1, 9, 1, 91, '2019-12-10 00:40:32', '2019-12-25 22:15:45'),
(66, 'Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue.', 3, 7, 1, 9, 89, 10, '2019-09-17 18:58:19', '2020-05-22 23:06:46'),
(67, 'Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.', 3, 47, 1, 8, 75, 68, '2020-05-02 12:52:27', '2020-02-05 01:58:53'),
(68, 'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum.', 2, 96, 1, 7, 47, 57, '2019-09-03 20:58:32', '2019-09-07 23:22:52'),
(69, 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 3, 17, 1, 7, 29, 86, '2020-03-07 19:14:41', '2019-10-25 10:46:37'),
(70, 'Duis mattis egestas metus.', 3, 24, 1, 11, 63, 29, '2019-10-19 06:44:22', '2019-09-24 19:51:25'),
(71, 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', 2, 39, 1, 11, 67, 98, '2020-06-14 07:29:32', '2019-11-29 02:18:27'),
(72, 'Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus.', 3, 95, 1, 5, 35, 4, '2020-03-08 07:25:35', '2019-08-26 17:24:50'),
(73, 'Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim.', 3, 16, 1, 7, 42, 45, '2019-10-01 03:16:48', '2019-12-12 09:21:15'),
(74, 'Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 3, 86, 1, 4, 64, 94, '2019-08-31 20:34:51', '2020-04-30 14:13:13'),
(75, 'Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', 3, 54, 1, 9, 1, 68, '2019-09-14 05:35:10', '2019-11-10 20:58:20'),
(76, 'Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', 2, 32, 1, 11, 41, 82, '2020-05-27 10:37:23', '2020-01-09 13:02:45'),
(77, 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum.', 3, 68, 1, 8, 30, 97, '2019-12-28 08:56:25', '2019-11-12 13:46:00'),
(78, 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo.', 3, 77, 1, 2, 45, 99, '2019-11-20 04:36:12', '2019-10-05 01:54:26'),
(79, 'Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', 2, 23, 1, 9, 79, 31, '2020-01-28 19:28:03', '2020-02-20 15:03:38'),
(80, 'Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla.', 3, 79, 1, 6, 30, 75, '2020-05-27 00:42:49', '2019-11-17 20:32:23'),
(81, 'In quis justo. Maecenas rhoncus aliquam lacus. Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.', 1, 53, 1, 9, 7, 96, '2020-01-21 07:21:47', '2020-04-29 22:16:00'),
(82, 'Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius.', 3, 38, 1, 7, 2, 75, '2020-08-06 16:59:11', '2020-01-22 10:25:18'),
(83, 'Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', 1, 55, 1, 3, 58, 29, '2019-09-08 03:27:19', '2020-06-06 19:17:54'),
(84, 'Vivamus in felis eu sapien cursus vestibulum.', 2, 97, 1, 4, 47, 79, '2019-10-02 18:28:16', '2020-07-29 13:16:08'),
(85, 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', 3, 22, 1, 7, 79, 81, '2019-09-06 04:11:18', '2020-02-09 05:50:47'),
(86, 'Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros.', 2, 90, 1, 2, 22, 58, '2019-12-05 20:13:41', '2019-10-10 02:17:41'),
(87, 'Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', 2, 96, 1, 7, 46, 52, '2020-06-13 01:15:46', '2020-03-05 05:50:31'),
(88, 'Phasellus in felis. Donec semper sapien a libero. Nam dui.', 2, 84, 1, 7, 77, 61, '2019-11-06 03:01:24', '2020-07-18 03:25:06'),
(89, 'Integer pede justo, lacinia eget, tincidunt eget, tempus vel, pede. Morbi porttitor lorem id ligula.', 1, 83, 1, 8, 35, 9, '2020-07-16 21:47:19', '2020-04-24 18:23:01'),
(90, 'Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque.', 3, 88, 1, 4, 11, 5, '2019-10-20 09:52:16', '2019-09-18 18:47:26'),
(91, 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo.', 3, 79, 1, 7, 29, 61, '2019-08-23 08:12:55', '2019-10-06 22:00:28'),
(92, 'Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est.', 1, 12, 1, 2, 53, 99, '2020-02-26 21:11:33', '2019-11-01 08:58:05'),
(93, 'Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', 1, 24, 1, 2, 1, 92, '2020-07-24 10:55:59', '2019-10-07 05:15:54'),
(94, 'Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus.', 1, 64, 1, 2, 61, 16, '2020-06-29 07:49:29', '2020-06-15 19:35:00'),
(95, 'Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices.', 1, 98, 1, 9, 64, 74, '2020-04-28 07:29:52', '2020-05-06 16:59:20'),
(96, 'Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum.', 2, 36, 1, 3, 68, 82, '2020-01-31 13:26:43', '2020-02-14 21:34:48'),
(97, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend.', 1, 17, 1, 9, 40, 54, '2019-08-13 05:42:29', '2019-10-19 05:15:40'),
(98, 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus.', 1, 6, 1, 11, 81, 80, '2019-09-14 21:23:47', '2019-10-05 10:05:11'),
(99, 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus.', 2, 89, 1, 5, 5, 40, '2019-12-19 15:12:23', '2019-12-30 02:08:02'),
(100, 'Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', 2, 63, 1, 1, 53, 58, '2020-04-07 09:09:38', '2019-09-08 00:05:59'),
(101, 'Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti.', 3, 72, 1, 1, 22, 72, '2019-09-06 21:03:33', '2020-03-29 06:27:15'),
(102, 'Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.', 2, 96, 1, 12, 87, 1, '2020-01-14 11:10:20', '2019-12-21 07:38:26'),
(103, 'Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum.', 1, 48, 1, 10, 23, 29, '2020-07-26 03:29:58', '2019-08-20 05:24:11'),
(104, 'Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo. In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 2, 35, 1, 9, 9, 88, '2020-02-13 23:29:49', '2020-02-02 15:26:14'),
(105, 'In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem.', 1, 32, 1, 3, 76, 36, '2019-08-19 00:21:15', '2020-01-08 12:34:06'),
(106, 'Vivamus tortor. Duis mattis egestas metus. Aenean fermentum.', 1, 16, 1, 3, 33, 51, '2020-01-04 11:42:44', '2019-09-09 07:03:02'),
(107, 'Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat.', 2, 72, 1, 11, 32, 93, '2020-03-28 20:18:06', '2019-10-01 00:29:25'),
(108, 'Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo.', 3, 20, 1, 7, 1, 89, '2020-04-10 23:40:37', '2020-06-23 02:54:26'),
(109, 'Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis.', 1, 55, 1, 5, 60, 27, '2020-01-09 01:47:01', '2020-06-12 01:05:34'),
(110, 'Sed accumsan felis. Ut at dolor quis odio consequat varius.', 3, 59, 1, 5, 54, 54, '2019-09-18 09:24:32', '2019-09-13 12:43:33'),
(111, 'Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst.', 1, 56, 1, 8, 84, 52, '2020-01-21 20:59:04', '2019-12-08 17:24:34'),
(112, 'Donec ut dolor.', 1, 65, 1, 8, 1, 8, '2020-04-13 05:32:19', '2020-04-25 04:19:36'),
(113, 'Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.', 1, 67, 1, 7, 51, 38, '2020-01-04 23:48:24', '2020-03-10 19:38:26'),
(114, 'Suspendisse potenti. Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue. Vestibulum rutrum rutrum neque.', 3, 37, 1, 10, 1, 89, '2020-02-09 15:26:26', '2020-01-10 18:09:33'),
(115, 'Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla.', 1, 23, 1, 3, 6, 81, '2019-10-25 01:11:48', '2020-06-08 00:22:29'),
(116, 'Pellentesque ultrices mattis odio.', 2, 42, 1, 8, 6, 29, '2020-04-06 06:28:08', '2020-03-18 21:54:22'),
(117, 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', 3, 74, 1, 4, 87, 65, '2019-10-28 20:25:50', '2019-10-16 07:22:05'),
(118, 'Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus.', 2, 46, 1, 4, 94, 16, '2020-07-12 04:38:45', '2019-09-03 19:37:57'),
(119, 'Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia.', 3, 18, 1, 4, 41, 15, '2020-03-24 14:37:15', '2019-12-26 18:39:22'),
(120, 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh.', 1, 14, 1, 11, 68, 89, '2020-04-25 15:33:35', '2020-08-03 11:35:52'),
(121, 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst.', 3, 90, 1, 4, 98, 3, '2019-10-21 11:54:12', '2019-12-20 04:49:16'),
(122, 'In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', 1, 23, 1, 12, 61, 65, '2019-11-13 00:01:36', '2020-01-19 17:40:50'),
(123, 'Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero.', 3, 26, 1, 5, 90, 7, '2019-10-12 18:17:17', '2019-11-10 10:55:09'),
(124, 'Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus.', 1, 19, 1, 6, 41, 76, '2020-06-21 07:30:30', '2020-02-26 02:53:35'),
(125, 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem.', 1, 23, 1, 10, 90, 85, '2019-11-27 18:07:59', '2020-07-16 17:07:22'),
(126, 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst.', 1, 64, 1, 1, 6, 99, '2020-03-14 04:25:04', '2019-09-27 09:02:30'),
(127, 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', 3, 13, 1, 5, 59, 1, '2019-12-30 03:20:04', '2019-08-17 02:38:09'),
(128, 'Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus.', 2, 61, 1, 1, 65, 45, '2020-05-08 22:32:50', '2020-05-16 12:57:25'),
(129, 'Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.', 1, 86, 1, 8, 50, 59, '2020-05-04 17:40:27', '2019-11-08 05:41:37'),
(130, 'Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum.', 3, 30, 1, 2, 35, 56, '2020-01-29 00:18:12', '2020-07-29 04:08:47'),
(131, 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio.', 3, 28, 1, 9, 50, 5, '2020-01-23 15:23:28', '2020-04-10 11:25:07'),
(132, 'In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus.', 1, 33, 1, 12, 1, 90, '2019-08-29 11:12:47', '2020-02-16 00:28:16'),
(133, 'In blandit ultrices enim. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.', 3, 97, 1, 2, 31, 52, '2020-06-08 04:22:52', '2020-01-19 19:56:23'),
(134, 'Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum.', 1, 62, 1, 11, 1, 65, '2020-02-11 18:29:04', '2020-05-15 14:18:16'),
(135, 'In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt.', 1, 70, 1, 6, 99, 16, '2019-10-16 11:42:27', '2020-01-26 06:19:39'),
(136, 'Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti.', 1, 84, 1, 7, 2, 97, '2020-06-09 12:33:40', '2020-05-22 08:57:26'),
(137, 'Donec semper sapien a libero. Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 3, 10, 1, 10, 82, 92, '2020-03-07 20:35:12', '2020-02-06 20:40:47'),
(138, 'Fusce consequat. Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante.', 2, 93, 1, 11, 79, 9, '2020-03-11 08:53:19', '2019-09-24 00:31:38'),
(139, 'Pellentesque viverra pede ac diam. Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam.', 3, 90, 1, 12, 42, 44, '2019-11-02 03:57:55', '2020-04-01 20:28:09'),
(140, 'Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui.', 3, 42, 1, 12, 73, 43, '2020-06-29 04:00:18', '2020-07-15 05:18:23'),
(141, 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti.', 3, 18, 1, 5, 83, 87, '2020-01-02 05:39:11', '2020-02-11 11:10:31'),
(142, 'Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc.', 3, 73, 1, 4, 41, 75, '2019-11-02 04:05:59', '2019-09-18 08:37:03'),
(143, 'Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus.', 2, 100, 1, 8, 26, 77, '2019-09-26 02:34:44', '2020-03-04 13:13:50'),
(144, 'Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est.', 1, 39, 1, 2, 1, 39, '2019-08-14 21:42:41', '2019-08-10 00:20:23'),
(145, 'Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius.', 1, 78, 1, 6, 47, 78, '2019-10-31 02:43:02', '2020-01-25 20:56:45'),
(146, 'Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', 1, 21, 1, 4, 1, 19, '2020-03-14 10:16:43', '2019-08-28 08:06:12'),
(147, 'Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem.', 3, 50, 1, 6, 1, 97, '2020-06-07 04:24:16', '2019-12-23 16:35:25'),
(148, 'Proin risus.', 2, 73, 1, 4, 72, 2, '2020-07-30 06:27:44', '2020-05-27 01:47:47'),
(149, 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim.', 3, 15, 1, 6, 48, 54, '2020-05-24 16:09:56', '2020-07-24 10:36:08'),
(150, 'Morbi quis tortor id nulla ultrices aliquet.', 3, 67, 1, 12, 9, 90, '2020-06-20 21:28:42', '2020-03-23 03:18:45'),
(151, '<p>test hwllo world&nbsp;</p>', 1, 115, 1, 2, 1, 45, '2020-08-07 11:35:23', '2020-08-07 11:35:23');

-- --------------------------------------------------------

--
-- Table structure for table `scholar_question_images`
--

CREATE TABLE `scholar_question_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question_id` int(11) NOT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `scholar_question_options`
--

CREATE TABLE `scholar_question_options` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question_id` int(11) NOT NULL,
  `option` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `scholar_question_options`
--

INSERT INTO `scholar_question_options` (`id`, `question_id`, `option`, `created_at`, `updated_at`) VALUES
(5, 979, 'Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis.', '2020-03-18 16:26:14', '2020-07-05 09:04:31'),
(6, 803, 'Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue.', '2020-06-20 23:57:36', '2019-12-21 10:22:46'),
(8, 32, 'Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus.', '2020-04-17 15:01:31', '2019-11-20 07:44:29'),
(9, 980, 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor.', '2020-01-28 07:22:08', '2020-07-14 10:30:56'),
(12, 339, 'Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst.', '2020-06-19 03:40:05', '2020-04-18 22:09:04'),
(13, 464, 'Integer tincidunt ante vel ipsum. Praesent blandit lacinia erat. Vestibulum sed magna at nunc commodo placerat. Praesent blandit.', '2020-01-21 04:42:12', '2020-02-02 06:07:57'),
(14, 881, 'Proin at turpis a pede posuere nonummy.', '2020-01-21 11:41:24', '2020-02-02 13:35:00'),
(16, 73, 'Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2020-03-20 15:36:29', '2019-09-06 00:01:00'),
(17, 211, 'Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo.', '2020-05-01 10:21:28', '2020-01-24 05:34:27'),
(18, 502, 'Cras pellentesque volutpat dui. Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam.', '2019-11-03 00:35:48', '2019-08-11 22:43:41'),
(19, 673, 'Aenean fermentum. Donec ut mauris eget massa tempor convallis. Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh.', '2019-12-25 06:17:01', '2019-09-03 11:53:57'),
(21, 400, 'Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.', '2020-04-18 09:21:47', '2019-10-11 04:03:08'),
(22, 490, 'Nulla ut erat id mauris vulputate elementum. Nullam varius.', '2020-04-25 00:45:22', '2019-12-30 17:24:16'),
(24, 260, 'Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus.', '2019-12-15 20:29:50', '2020-05-10 01:15:45'),
(25, 427, 'Fusce consequat. Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus.', '2020-07-14 20:38:54', '2020-03-20 06:12:51'),
(27, 935, 'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis. Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum.', '2020-08-02 15:29:15', '2019-09-16 19:42:02'),
(28, 859, 'Morbi quis tortor id nulla ultrices aliquet. Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo. Pellentesque viverra pede ac diam.', '2019-10-31 12:27:53', '2020-07-31 10:52:10'),
(29, 207, 'Donec dapibus. Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante.', '2019-09-28 05:12:11', '2020-01-20 23:56:48'),
(35, 721, 'Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit.', '2020-04-26 11:37:32', '2020-06-13 16:21:25'),
(37, 929, 'Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio.', '2020-08-04 01:35:23', '2020-01-15 11:24:40'),
(39, 868, 'Vivamus vel nulla eget eros elementum pellentesque.', '2019-11-18 07:14:08', '2019-11-17 07:40:20'),
(41, 692, 'In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna.', '2020-07-20 20:54:53', '2019-11-28 07:35:40'),
(42, 400, 'Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue.', '2020-03-20 23:37:48', '2019-10-05 23:58:59'),
(43, 344, 'Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci.', '2019-08-10 16:29:27', '2020-05-29 16:05:42'),
(47, 525, 'Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci. Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum. Integer a nibh. In quis justo.', '2019-11-04 02:40:17', '2020-08-04 03:53:41'),
(50, 19, 'Sed ante. Vivamus tortor. Duis mattis egestas metus. Aenean fermentum. Donec ut mauris eget massa tempor convallis.', '2020-05-18 06:14:46', '2020-05-21 10:27:42'),
(52, 980, 'Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat.', '2019-12-23 14:54:10', '2020-03-08 02:58:41'),
(54, 474, 'Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2019-11-06 18:37:26', '2020-07-12 04:43:10'),
(55, 524, 'Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2019-11-02 23:46:35', '2020-07-20 02:12:04'),
(58, 562, 'Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2020-04-08 11:38:46', '2020-04-06 05:06:49'),
(59, 672, 'Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem.', '2020-06-22 16:13:07', '2019-12-02 01:30:03'),
(62, 492, 'Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus.', '2019-10-30 16:19:49', '2020-05-13 08:42:40'),
(63, 927, 'Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy. Integer non velit.', '2020-07-21 12:22:06', '2020-04-27 02:44:22'),
(64, 379, 'Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque.', '2020-02-29 19:20:57', '2020-07-13 20:05:13'),
(65, 65, 'Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus.', '2020-05-21 03:11:32', '2019-11-19 21:33:19'),
(67, 864, 'In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl. Nunc nisl. Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus.', '2019-10-10 11:06:47', '2019-10-17 04:11:05'),
(68, 194, 'Fusce posuere felis sed lacus.', '2020-06-03 22:02:09', '2019-11-23 22:54:41'),
(70, 965, 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh.', '2019-10-22 07:10:58', '2019-09-05 15:51:16'),
(71, 156, 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio.', '2019-11-22 02:58:16', '2019-12-18 11:35:07'),
(72, 696, 'Mauris lacinia sapien quis libero.', '2020-08-05 04:03:13', '2019-11-06 02:07:10'),
(74, 449, 'Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius.', '2019-10-29 18:55:43', '2020-07-23 20:20:53'),
(77, 326, 'Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', '2019-10-06 07:18:25', '2020-05-07 09:39:02'),
(80, 297, 'Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus. Curabitur at ipsum ac tellus semper interdum.', '2019-09-25 18:06:21', '2020-03-05 20:44:53'),
(82, 933, 'Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum.', '2019-12-09 21:53:06', '2020-03-06 01:02:55'),
(85, 235, 'Etiam vel augue. Vestibulum rutrum rutrum neque. Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo.', '2020-03-05 22:07:59', '2019-09-18 08:48:41'),
(86, 934, 'Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2020-02-26 18:26:13', '2019-12-06 02:17:32'),
(87, 644, 'Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat.', '2020-08-03 17:30:33', '2020-05-01 05:42:59'),
(88, 514, 'In congue. Etiam justo. Etiam pretium iaculis justo.', '2019-09-03 18:43:45', '2020-07-21 22:41:43'),
(89, 255, 'Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti.', '2020-06-05 18:34:06', '2019-12-16 21:37:35'),
(90, 640, 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl.', '2020-06-15 17:20:53', '2019-08-07 20:06:33'),
(92, 698, 'Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat.', '2020-05-27 15:22:46', '2020-01-02 07:10:37'),
(93, 421, 'Vivamus in felis eu sapien cursus vestibulum. Proin eu mi. Nulla ac enim. In tempor, turpis nec euismod scelerisque, quam turpis adipiscing lorem, vitae mattis nibh ligula nec sem. Duis aliquam convallis nunc. Proin at turpis a pede posuere nonummy.', '2019-11-30 22:04:15', '2020-07-21 14:09:19'),
(94, 381, 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris.', '2020-08-03 00:47:44', '2020-04-10 14:42:05'),
(95, 307, 'Morbi a ipsum. Integer a nibh. In quis justo. Maecenas rhoncus aliquam lacus.', '2020-07-08 12:50:45', '2020-01-04 07:22:07'),
(96, 377, 'Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat.', '2020-02-06 08:34:43', '2020-01-10 20:04:25'),
(99, 536, 'Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo.', '2020-01-30 00:35:30', '2019-12-29 16:27:44'),
(100, 660, 'Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat. Nulla tempus. Vivamus in felis eu sapien cursus vestibulum. Proin eu mi.', '2020-04-19 21:36:20', '2019-12-28 16:03:22'),
(101, 126, 'Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh.', '2020-02-05 15:23:40', '2020-01-20 06:46:29'),
(104, 446, 'Nulla justo. Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros.', '2019-09-23 14:06:16', '2020-03-14 14:03:06'),
(105, 452, 'Maecenas tristique, est et tempus semper, est quam pharetra magna, ac consequat metus sapien ut nunc.', '2020-05-01 22:22:52', '2019-10-12 18:30:25'),
(109, 632, 'Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh. In hac habitasse platea dictumst. Aliquam augue quam, sollicitudin vitae, consectetuer eget, rutrum at, lorem. Integer tincidunt ante vel ipsum.', '2020-01-10 18:30:15', '2019-12-11 22:50:05'),
(110, 620, 'Aliquam erat volutpat. In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi.', '2019-09-06 14:09:25', '2020-05-05 04:57:08'),
(111, 459, 'Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis.', '2019-11-26 22:48:49', '2019-12-18 20:29:16'),
(112, 820, 'Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi.', '2019-11-24 14:37:17', '2020-03-06 21:28:34'),
(113, 781, 'Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis. Morbi odio odio, elementum eu, interdum eu, tincidunt in, leo. Maecenas pulvinar lobortis est. Phasellus sit amet erat.', '2020-06-11 02:25:02', '2019-08-24 21:55:59'),
(114, 151, 'formControlName=\"question\"1', '2020-08-07 11:35:23', '2020-08-07 11:35:23'),
(115, 151, 'formControlName=\"question\"2', '2020-08-07 11:35:23', '2020-08-07 11:35:23'),
(116, 151, 'formControlName=\"question\"3', '2020-08-07 11:35:23', '2020-08-07 11:35:23'),
(117, 151, 'formControlName=\"question\"4', '2020-08-07 11:35:23', '2020-08-07 11:35:23');

-- --------------------------------------------------------

--
-- Table structure for table `scholar_question_option_images`
--

CREATE TABLE `scholar_question_option_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `option_id` int(11) NOT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `scholar_question_solutions`
--

CREATE TABLE `scholar_question_solutions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `question_id` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `scholar_question_solutions`
--

INSERT INTO `scholar_question_solutions` (`id`, `question_id`, `description`, `image_path`, `created_at`, `updated_at`) VALUES
(1, 151, 'formControlName=\"question\"', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `scholar_random_selection_settings`
--

CREATE TABLE `scholar_random_selection_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `level_id` int(11) NOT NULL,
  `percent_selection` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `scholar_random_selection_settings`
--

INSERT INTO `scholar_random_selection_settings` (`id`, `level_id`, `percent_selection`, `created_at`, `updated_at`) VALUES
(1, 1, 25, NULL, NULL),
(2, 2, 50, NULL, NULL),
(3, 3, 25, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `scholar_subjects`
--

CREATE TABLE `scholar_subjects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `scholar_subjects`
--

INSERT INTO `scholar_subjects` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, '12th Physics', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `scholar_test_questions`
--

CREATE TABLE `scholar_test_questions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `test_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `scholar_test_questions`
--

INSERT INTO `scholar_test_questions` (`id`, `test_id`, `question_id`, `created_at`, `updated_at`) VALUES
(1, 1, 5, NULL, NULL),
(2, 1, 6, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `scholar_test_question_papers`
--

CREATE TABLE `scholar_test_question_papers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subject_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `type_wise_id` int(11) NOT NULL,
  `no_of_questions` int(11) NOT NULL,
  `time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `scholar_types`
--

CREATE TABLE `scholar_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `scholar_types`
--

INSERT INTO `scholar_types` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Full Test', NULL, NULL),
(2, 'Group wise Test', NULL, NULL),
(3, 'Chapter wise Test', NULL, NULL),
(4, 'Topic wise Test', NULL, NULL),
(5, 'Full Test', NULL, NULL),
(6, 'Group wise Test', NULL, NULL),
(7, 'Chapter wise Test', NULL, NULL),
(8, 'Topic wise Test', NULL, NULL),
(9, 'Full Test', NULL, NULL),
(10, 'Group wise Test', NULL, NULL),
(11, 'Chapter wise Test', NULL, NULL),
(12, 'Topic wise Test', NULL, NULL),
(13, 'Full Test', NULL, NULL),
(14, 'Group wise Test', NULL, NULL),
(15, 'Chapter wise Test', NULL, NULL),
(16, 'Topic wise Test', NULL, NULL),
(17, 'Full Test', NULL, NULL),
(18, 'Group wise Test', NULL, NULL),
(19, 'Chapter wise Test', NULL, NULL),
(20, 'Topic wise Test', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `scholar_chapters`
--
ALTER TABLE `scholar_chapters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_chapter_sub_topics`
--
ALTER TABLE `scholar_chapter_sub_topics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_chapter_topics`
--
ALTER TABLE `scholar_chapter_topics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_levels`
--
ALTER TABLE `scholar_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_migrations`
--
ALTER TABLE `scholar_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_question_banks`
--
ALTER TABLE `scholar_question_banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_question_images`
--
ALTER TABLE `scholar_question_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_question_options`
--
ALTER TABLE `scholar_question_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_question_option_images`
--
ALTER TABLE `scholar_question_option_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_question_solutions`
--
ALTER TABLE `scholar_question_solutions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_random_selection_settings`
--
ALTER TABLE `scholar_random_selection_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_subjects`
--
ALTER TABLE `scholar_subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_test_questions`
--
ALTER TABLE `scholar_test_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_test_question_papers`
--
ALTER TABLE `scholar_test_question_papers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `scholar_types`
--
ALTER TABLE `scholar_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `scholar_chapters`
--
ALTER TABLE `scholar_chapters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `scholar_chapter_sub_topics`
--
ALTER TABLE `scholar_chapter_sub_topics`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT for table `scholar_chapter_topics`
--
ALTER TABLE `scholar_chapter_topics`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;
--
-- AUTO_INCREMENT for table `scholar_levels`
--
ALTER TABLE `scholar_levels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `scholar_migrations`
--
ALTER TABLE `scholar_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `scholar_question_banks`
--
ALTER TABLE `scholar_question_banks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=152;
--
-- AUTO_INCREMENT for table `scholar_question_images`
--
ALTER TABLE `scholar_question_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `scholar_question_options`
--
ALTER TABLE `scholar_question_options`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;
--
-- AUTO_INCREMENT for table `scholar_question_option_images`
--
ALTER TABLE `scholar_question_option_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `scholar_question_solutions`
--
ALTER TABLE `scholar_question_solutions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `scholar_random_selection_settings`
--
ALTER TABLE `scholar_random_selection_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `scholar_subjects`
--
ALTER TABLE `scholar_subjects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `scholar_test_questions`
--
ALTER TABLE `scholar_test_questions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `scholar_test_question_papers`
--
ALTER TABLE `scholar_test_question_papers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `scholar_types`
--
ALTER TABLE `scholar_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
