<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->get('/', function () use ($router) {
    //return $router->app->version();
    echo "hi";
});
$router->post('g', 'AppController@generateTp');
$router->get('get-all-data/{type}', 'AppController@showAllData');
$router->get('get-data/{type}/{id}', 'AppController@showData');
$router->post('add-data', 'AppController@addData');
$router->post('delete-data', 'AppController@deleteData');
$router->post('update-data', 'AppController@updateData');
$router->post('delete-all-data', 'AppController@deleteAllData');

$router->get('get-subject-chapter-data/{subject_id}', 'AppController@getSubjectChapters');
$router->get('get-chapter-topic-data/{chapter_id}', 'AppController@getChapterTopics');
$router->get('get-topic-subtopic-data/{topic_id}', 'AppController@getTopicSubTopics');
$router->get('test/{subject_id}', 'AppController@getChaptersData');
$router->get('test2/{chapter_id}', 'AppController@getChapterTopicsData');
$router->get('test3/{topic_id}', 'AppController@getTopicQuestionsData');
$router->get('export', 'AppController@exp');