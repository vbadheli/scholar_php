<?php

use Illuminate\Database\Seeder;
use App\Level;
class LevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Level::insert(['title'=>'Hard']);
        Level::insert(['title'=>'Medium']);
        Level::insert(['title'=>'Easy']);
    }
}
