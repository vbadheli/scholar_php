<?php

use Illuminate\Database\Seeder;
use App\Type;
class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Type::insert(['title'=>'Full Test']);
        Type::insert(['title'=>'Group wise Test']);
        Type::insert(['title'=>'Chapter wise Test']);
        Type::insert(['title'=>'Topic wise Test']);
    }
}
