<?php

use Illuminate\Database\Seeder;
use \App\RandomSelectionSetting;

class random_selection_setting_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RandomSelectionSetting::insert(["level_id"=> 1, "percent_selection"=>25]);
        RandomSelectionSetting::insert(["level_id"=> 2, "percent_selection"=>50]);
        RandomSelectionSetting::insert(["level_id"=> 3, "percent_selection"=>25]);
    }
}
