<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestQuestionPapersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_question_papers', function (Blueprint $table) {
            $table->id();
            $table->integer('subject_id');
            // $table->integer('chapter_id');
            // $table->integer('chapter_topic_id');
            // $table->integer('chapter_sub_topic_id');
            $table->integer('type_id');
            $table->integer('type_wise_id');
            $table->integer('no_of_questions');
            $table->string('time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_test_question_papers');
    }
}
